# sh/ksh initialization
if [ -f /etc/ksh.kshrc/ ]; then
	. /etc/ksh.kshrc
fi

PS1='($PWD)> '

export PS1
